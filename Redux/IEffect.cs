﻿using System;

namespace Redux
{
    public interface IEffect<T>
    {
        void Connect(IObservable<StateActionPair<T, IAction>> stateActionStream);

        void Disconnect();
    }
}