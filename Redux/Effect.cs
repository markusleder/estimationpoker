﻿using System;
using System.Reactive.Linq;

namespace Redux
{
    public abstract class Effect<T> : IEffect<T>
    {
        public void Connect(IObservable<StateActionPair<T, IAction>> stateActionStream)
        {
            ActionStateStream = stateActionStream;
            Subscribe();
        }

        public void Disconnect()
        {
            Unsubscribe();
            ActionStateStream = null;
        }

        protected IObservable<StateActionPair<T, IAction>> ActionStateStream { get; private set; }

        protected IObservable<T> State => this.ActionStateStream.Select(pair => pair.State);

        protected IObservable<IAction> Action => this.ActionStateStream.Select(pair => pair.Action);

        protected abstract void Subscribe();

        protected abstract void Unsubscribe();
    }
}
