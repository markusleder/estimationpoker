﻿using System;

namespace Redux
{
    public interface IActionDispatcher : IObservable<IAction>
    {
        void Dispatch(IAction action);
    }
}