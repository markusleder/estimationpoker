﻿using System;

namespace Redux
{
    public abstract class Immutable<T> where T : Immutable<T>
    {
        protected T Update(Action<T> updateAction)
        {
            var clone = (T)MemberwiseClone();
            updateAction(clone);
            return clone;
        }

        protected T Update(Action<T> updateAction, Func<bool> updateCheck)
        {
            if (!updateCheck())
            {
                return (T)this;
            }

            return Update(updateAction);
        }
    }
}
