# Estimation Poker #

an estimation tool for agile projects.

________                _________     .__                         
\______ \   ___________/   _____/__ __|  |_________   ___________ 
 |    |  \_/ __ \_  __ \_____  \|  |  \  |  \_  __ \_/ __ \_  __ \
 |    `   \  ___/|  | \/        \  |  /   Y  \  | \/\  ___/|  | \/
/_______  /\___  >__| /_______  /____/|___|  /__|    \___  >__|   
        \/     \/             \/           \/            \/       

### Try out live ###
https://estimationpoker.azurewebsites.net/

![alt text](EstimationPoker/wwwroot/images/aScreen.JPG "Logo")

### Technology ###

* Microsoft.NETCore.App 3.1.0
* Server-side Blazor 
* Visual Studio 2019 with C#

### Stories ###

* create and join a session
* vote (fibonacci scale)
* show votes

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* lem

### Credits ###

* ASCII Art http://patorjk.com/software/taag
