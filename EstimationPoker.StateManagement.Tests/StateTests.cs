using System.Linq;
using EstimationPoker.StateManagement.Actions;
using EstimationPoker.StateManagement.Reducer;
using EstimationPoker.StateManagement.State;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EstimationPoker.StateManagement.Tests
{
    [TestClass]
    public class StateTests
    {
        private string _voterName = "lem";

        [TestMethod]
        public void JoinSession_NotYetJoined_IsNowJoined()
        {
            var applicationState = CreateState();

            var other = Reducers.Reduce(applicationState, new JoinVotingSessionAction { VoterName = _voterName });

            Assert.AreEqual(1, other.VotingSession.Votes.Count);
        }

        [TestMethod]
        public void JoinSession_AlreadyJoined_IsIdempotent()
        {
            var applicationState = CreateStateWithOneVoter();

            var other = Reducers.Reduce(applicationState, new JoinVotingSessionAction { VoterName = _voterName });

            Assert.AreEqual(1, other.VotingSession.Votes.Count);
        }

        [TestMethod]
        public void ExitSession_WhenJoined_ThenRemoved()
        {
            var applicationState = CreateStateWithOneVoter();

            var other = Reducers.Reduce(applicationState, new ExitVotingSessionAction() { VoterName = _voterName });

            Assert.AreEqual(0, other.VotingSession.Votes.Count);
        }

        [TestMethod]
        public void ExitSession_WhenNotJoined_IsIdempotent()
        {
            var applicationState = CreateState();

            var other = Reducers.Reduce(applicationState, new ExitVotingSessionAction() { VoterName = _voterName });

            Assert.AreEqual(0, other.VotingSession.Votes.Count);
        }

        [TestMethod]
        public void ClearVotes_WhenSeveralHaveJoined_ValuesAreSetBack()
        {
            var applicationState = CreateStateWithOneVoter();
            applicationState = applicationState.UpdateVotingSession(
                applicationState.VotingSession.JoinSession("bla").VoteVotingSession("bla", 8)
            );
            
            var other = Reducers.Reduce(applicationState, new ClearVotesAction());

            Assert.AreEqual(2, other.VotingSession.Votes.Count);
            Assert.AreNotEqual(13, other.VotingSession.Votes.First().Value);
            Assert.AreNotEqual(8, other.VotingSession.Votes.Skip(1).First().Value);
        }

        private static ApplicationState CreateState()
        {
            var applicationState = new ApplicationState()
                .UpdateVotingSession(new VotingSessionState()
                    .UpdateFinalValue(int.MinValue)
                );
                
            return applicationState;
        }

        private ApplicationState CreateStateWithOneVoter()
        {
            var applicationState = new ApplicationState()
                .UpdateVotingSession(
                    new VotingSessionState()
                        .JoinSession(_voterName)
                        .VoteVotingSession(_voterName, 13)
                    );

            return applicationState;
        }
    }
}