using System.Collections.Immutable;
using System.Linq;
using EstimationPoker.StateManagement.Actions;
using EstimationPoker.StateManagement.Reducer;
using EstimationPoker.StateManagement.State;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EstimationPoker.StateManagement.Tests
{
    [TestClass]
    public class ReducerTests
    {
        [TestMethod]
        public void ReduceApplicationState_WithFinalValue_NewInstanceWithValue()
        {
            var applicationState = new ApplicationState()
                .UpdateVotingSession(
                    new VotingSessionState()
                        .UpdateFinalValue(int.MinValue)
                );

            var other = Reducers.Reduce(applicationState, new FinalValueAction {Value = 13});

            Assert.AreEqual(13, other.VotingSession.FinalValue);
            Assert.AreEqual(0, other.VotingSession.Votes.Count);
        }

        [TestMethod]
        public void ReduceVotingSession_WithStory_NewInstanceWithStory()
        {
            var applicationState = CreateApplicationState();

            var other = Reducers.Reduce(applicationState, 
                new UpdateStoryAction { Story = "otherStory", Description = applicationState.VotingSession.Description });

            Assert.AreEqual("theStory", applicationState.VotingSession.Story);
            Assert.AreEqual("otherStory", other.VotingSession.Story);
            Assert.AreEqual(applicationState.VotingSession.Description, other.VotingSession.Description);
        }


        [TestMethod]
        public void ReduceVote_WithValue_NewInstanceWithValue()
        {
            var applicationState = CreateApplicationState();

            var voterName = applicationState.VotingSession.Votes.First().VoterName;
            var other = Reducers.Reduce(applicationState,
                new VoteVotingAction { Value = 13, VoterName = voterName });

            Assert.AreEqual(voterName, other.VotingSession.Votes.First().VoterName);
            Assert.AreEqual(13, other.VotingSession.Votes.First().Value);
            Assert.AreEqual(applicationState.VotingSession.Votes.Count, other.VotingSession.Votes.Count);
        }

        private static ApplicationState CreateApplicationState()
        {
            var applicationState = new ApplicationState()
                .UpdateVotingSession(
                    new VotingSessionState()
                        .UpdateStory("theStory")
                        .UpdateSessionId("s")
                        .UpdateDescription("D")
                        .JoinSession("theVoter")
                );

            return applicationState;
        }
    }
}