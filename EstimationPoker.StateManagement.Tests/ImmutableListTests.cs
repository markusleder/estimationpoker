using System;
using System.Collections.Immutable;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EstimationPoker.StateManagement.Tests
{
    [TestClass]
    public class ImmutableListTests
    {
        [TestMethod]
        public void Add_ReturnsNewList()
        {
            var target = ImmutableList<TestItem>.Empty;

            var testItem = new TestItem {Name = Guid.NewGuid().ToString()};
            var newTarget = target.Add(testItem);

            Assert.IsTrue(target.Count == 0);
            Assert.IsTrue(newTarget.Count == 1);
            Assert.AreNotSame(target, newTarget);
        }

        [TestMethod]
        public void Add_ReturnsNewList_WithSameItems()
        {
            var target = ImmutableList<TestItem>.Empty;

            var testItem1 = new TestItem { Name = Guid.NewGuid().ToString() };
            var newTarget1 = target.Add(testItem1);

            var testItem2 = new TestItem { Name = Guid.NewGuid().ToString() };
            var newTarget2 = newTarget1.Add(testItem2);

            Assert.AreEqual(newTarget1.First().GetHashCode(), newTarget2.First().GetHashCode());
            Assert.AreEqual(newTarget1.First().Name, newTarget2.First().Name);
        }
    }
}
