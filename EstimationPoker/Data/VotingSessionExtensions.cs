﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace EstimationPoker.Data
{
    public static class VotingSessionExtensions
    {
        public static Dictionary<int, int> GroupAndCount(this VotingSession votingSession)
        {
            var counts = new Dictionary<int, int>();
            foreach (var v in Fibonacci.Enumerate())
            {
                counts.Add(v, 0);
            }

            // how many voters per value?
            var c = votingSession.Votes.Select(i => i.Value).GroupBy(g => g).Select(v => new KeyValuePair<int, int>(v.Key, v.Count()));
            
            foreach (var v in c)
            {
                counts[v.Key] = v.Value;
            }

            return counts;
        }

        public static string GetVote(this VotingSession votingSession, Vote v, string name)
        {
            // show my own vote - or when all voters have voted. Handle the "don't" know "?"
            return (votingSession.HasAllVotes() || v.IsMyVote(name)) ? (v.HasPreciseVote() ? v.Value.ToString() : "?") : "";
        }

        public static bool HasAllVotes(this VotingSession votingSession)
        {
            return votingSession.Votes.All(v => v.Value > Vote.None);
        }

        public static string GetAverage(this VotingSession votingSession)
        {
            var allPreciseVotes = votingSession.Votes.Where(v => v.HasPreciseVote()).ToList();
            if (!allPreciseVotes.Any())
            {
                return "?";
            }

            var average = allPreciseVotes.Average(v => v.Value);
            return $"{average:F1}";
        }
    }
}
