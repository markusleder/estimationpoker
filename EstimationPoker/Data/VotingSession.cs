﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EstimationPoker.Data
{
    public class VotingSession
    {
        public string SessionId { get; set; }

        public string Story { get; set; }

        public string Description { get; set; }

        public List<Vote> Votes { get; set; } = new List<Vote>();

        public void Join(string name)
        {
            lock (this)
            {
                if (!Votes.Any(v => v.VoterName.Equals(name, StringComparison.InvariantCultureIgnoreCase)))
                {
                    Votes.Add(new Vote {VoterName = name});
                }
            }
        }

        public void Vote(string name, int value)
        {
            lock (this)
            {
                var theVote = Votes.Single(v => v.VoterName.Equals(name));

                theVote.Timestamp = DateTime.Now;
                theVote.Value = value;
            }
        }

        public void Exit(string name)
        {
            lock (this)
            {
                var vote = Votes.SingleOrDefault(v => v.VoterName.Equals(name));
                if (vote != null)
                {
                    Votes.Remove(vote);
                }
            }
        }

        public void ClearVotes()
        {
            lock (this)
            {
                foreach (var vote in Votes)
                {
                    vote.Value = int.MinValue;
                }
            }
        }
    }
}
