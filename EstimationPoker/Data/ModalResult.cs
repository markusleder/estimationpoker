﻿namespace EstimationPoker.Data
{
    public enum ModalResult
    {
        None,
        Ok,
        Cancel,
        Yes,
        No
    }
}
