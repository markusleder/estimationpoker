﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EstimationPoker.Data
{
    public class Vote
    {
        [Required]
        [MinLength(3, ErrorMessage = "Name must be 3 characters or longer!")]
        public string VoterName { get; set; }

        public int Value { get; set; } = None;

        public DateTime Timestamp { get; set; } = DateTime.MinValue;

        public static int DontKnow => -1;

        public static int None => int.MinValue;
    }
}
