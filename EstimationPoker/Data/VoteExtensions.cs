﻿namespace EstimationPoker.Data
{
    public static class VoteExtensions
    {
        public static bool IsMyVote(this Vote v, string name)
        {
            return v.VoterName.Equals(name);
        }

        public static bool HasVote(this Vote v)
        {
            return v.Value > Vote.None;
        }

        public static bool HasPreciseVote(this Vote v)
        {
            return v.Value > Vote.DontKnow;
        }
    }
}
