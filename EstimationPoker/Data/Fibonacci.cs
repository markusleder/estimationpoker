﻿using System;
using System.Collections.Generic;

namespace EstimationPoker.Data
{
    public static class Fibonacci
    {
        public static IEnumerable<int> Enumerate(bool withoutFirst = false, bool withoutLast = false)
        {
            if (!withoutFirst) yield return 0;
            yield return 1;
            yield return 2;
            yield return 3;
            yield return 5;
            yield return 8;
            yield return 13;
            yield return 21;
            yield return 34;
            yield return 55;
            yield return 89;
            if (!withoutLast) yield return Vote.DontKnow;
        }
    }
}
