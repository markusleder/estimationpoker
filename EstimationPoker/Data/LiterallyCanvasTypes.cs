﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EstimationPoker.Data
{
    public class ShapeComparer : EqualityComparer<Shape>
    {
        public override bool Equals(Shape x, Shape y)
        {
            return x?.Id == y?.Id;
        }

        public override int GetHashCode(Shape obj)
        {
            return obj.GetHashCode();
        }
    }

    /// <summary>
    ///     https://app.quicktype.io/#l=cs&r=json2csharp
    /// </summary>
    public partial class Snapshot
    {
        [JsonProperty("colors")] public Colors Colors { get; set; }

        [JsonProperty("position")] public Position Position { get; set; }

        [JsonProperty("scale")] public long Scale { get; set; }

        [JsonProperty("shapes")] public List<Shape> Shapes { get; set; }

        [JsonProperty("backgroundShapes")] public List<object> BackgroundShapes { get; set; }

        [JsonProperty("imageSize")] public ImageSize ImageSize { get; set; }
    }

    public class Colors
    {
        [JsonProperty("primary")] public string Primary { get; set; }

        [JsonProperty("secondary")] public string Secondary { get; set; }

        [JsonProperty("background")] public string Background { get; set; }
    }

    public class ImageSize
    {
        [JsonProperty("width")] public string Width { get; set; }

        [JsonProperty("height")] public string Height { get; set; }
    }

    public class Position
    {
        [JsonProperty("x")] public long X { get; set; }

        [JsonProperty("y")] public long Y { get; set; }
    }

    public class Shape
    {
        [JsonProperty("className")] public string ClassName { get; set; }

        [JsonProperty("data")] public Data Data { get; set; }

        [JsonProperty("id")] public Guid Id { get; set; }
    }

    public class Data
    {
        [JsonProperty("order", NullValueHandling = NullValueHandling.Ignore)]
        public long? Order { get; set; }

        [JsonProperty("tailSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? TailSize { get; set; }

        [JsonProperty("smooth", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Smooth { get; set; }

        [JsonProperty("pointCoordinatePairs", NullValueHandling = NullValueHandling.Ignore)]
        public List<List<double>> PointCoordinatePairs { get; set; }

        [JsonProperty("smoothedPointCoordinatePairs", NullValueHandling = NullValueHandling.Ignore)]
        public List<List<double>> SmoothedPointCoordinatePairs { get; set; }

        [JsonProperty("pointSize", NullValueHandling = NullValueHandling.Ignore)]
        public long? PointSize { get; set; }

        [JsonProperty("pointColor", NullValueHandling = NullValueHandling.Ignore)]
        public string PointColor { get; set; }

        [JsonProperty("x1", NullValueHandling = NullValueHandling.Ignore)]
        public long? X1 { get; set; }

        [JsonProperty("y1", NullValueHandling = NullValueHandling.Ignore)]
        public double? Y1 { get; set; }

        [JsonProperty("x2", NullValueHandling = NullValueHandling.Ignore)]
        public long? X2 { get; set; }

        [JsonProperty("y2", NullValueHandling = NullValueHandling.Ignore)]
        public double? Y2 { get; set; }

        [JsonProperty("strokeWidth", NullValueHandling = NullValueHandling.Ignore)]
        public long? StrokeWidth { get; set; }

        [JsonProperty("color", NullValueHandling = NullValueHandling.Ignore)]
        public string Color { get; set; }

        [JsonProperty("capStyle", NullValueHandling = NullValueHandling.Ignore)]
        public string CapStyle { get; set; }

        [JsonProperty("dash")] public object Dash { get; set; }

        [JsonProperty("endCapShapes", NullValueHandling = NullValueHandling.Ignore)]
        public List<object> EndCapShapes { get; set; }

        [JsonProperty("x", NullValueHandling = NullValueHandling.Ignore)]
        public long? X { get; set; }

        [JsonProperty("y", NullValueHandling = NullValueHandling.Ignore)]
        public double? Y { get; set; }

        [JsonProperty("width", NullValueHandling = NullValueHandling.Ignore)]
        public long? Width { get; set; }

        [JsonProperty("height", NullValueHandling = NullValueHandling.Ignore)]
        public long? Height { get; set; }

        [JsonProperty("strokeColor", NullValueHandling = NullValueHandling.Ignore)]
        public string StrokeColor { get; set; }

        [JsonProperty("fillColor", NullValueHandling = NullValueHandling.Ignore)]
        public string FillColor { get; set; }

        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        [JsonProperty("font", NullValueHandling = NullValueHandling.Ignore)]
        public string Font { get; set; }

        [JsonProperty("forcedWidth", NullValueHandling = NullValueHandling.Ignore)]
        public long? ForcedWidth { get; set; }

        [JsonProperty("forcedHeight", NullValueHandling = NullValueHandling.Ignore)]
        public long? ForcedHeight { get; set; }

        [JsonProperty("v", NullValueHandling = NullValueHandling.Ignore)]
        public long? V { get; set; }
    }

    public partial class Snapshot
    {
        public static Snapshot FromJson(string json)
        {
            return JsonConvert.DeserializeObject<Snapshot>(json, Converter.Settings);
        }

        public static string ToJson(Snapshot self)
        {
            return JsonConvert.SerializeObject(self, Converter.Settings);
        }
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
            }
        };
    }
}
