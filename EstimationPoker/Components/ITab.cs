﻿using Microsoft.AspNetCore.Components;

namespace EstimationPoker.Components
{
    public interface ITab
    {
        RenderFragment ChildContent { get; }
    }
}
