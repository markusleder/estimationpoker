﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace EstimationPoker.Services
{
    public class DrawingHub : Hub
    {
        // ReSharper disable once UnusedMember.Global
        public async Task Join(string sessionId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, sessionId);
        }

        // ReSharper disable once UnusedMember.Global
        public async Task AddShape(string shape, string shapeId, string sessionId)
        {
            await Clients.Group(sessionId).SendAsync(Messages.AddShape, shape, shapeId);
        }
    }
}
