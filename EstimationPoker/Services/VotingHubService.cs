﻿using System;
using System.Threading.Tasks;
using EstimationPoker.Data;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;

namespace EstimationPoker.Services
{
    // https://github.com/conficient/BlazorChatSample/blob/master/BlazorChatSample.Shared/ChatClient.cs
    public class VotingHubService : IAsyncDisposable
    {
        private readonly string _hubUrl;
        private readonly NavigationManager _navigationManager;
        private readonly string _sessionId;
        private readonly string _username;

        private HubConnection _hubConnection;
        private bool _started;


        public VotingHubService(string sessionId, string username, NavigationManager navigationManager)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentException("username must be non-null");

            _sessionId = sessionId;
            _username = username;
            _hubUrl = "/votinghub";
            _navigationManager = navigationManager;
        }

        public async Task StartAsync()
        {
            if (!_started)
            {
                _hubConnection = new HubConnectionBuilder()
                    .WithUrl(_navigationManager.ToAbsoluteUri(_hubUrl))
                    .Build();

                _hubConnection.On<string, VotingSession>(Messages.UpdateVoting,
                    (user, message) =>
                    {
                        // Note: session handling not very optimized: votes from other sessions also arrive here
                        if (message.SessionId.Equals(_sessionId, StringComparison.InvariantCultureIgnoreCase))
                            MessageReceived?.Invoke(this,
                                new MessageReceivedEventArgs(Messages.UpdateVoting, user, message));
                    });
                _hubConnection.On<VotingSession>(Messages.UpdateStory,
                    message =>
                    {
                        if (message.SessionId.Equals(_sessionId, StringComparison.InvariantCultureIgnoreCase))
                            MessageReceived?.Invoke(this,
                                new MessageReceivedEventArgs(Messages.UpdateStory, string.Empty, message));
                    });

                await _hubConnection.StartAsync();
                _started = true;
            }
        }

        public event MessageReceivedEventHandler MessageReceived;

        public async Task SendVotingAsync(VotingSession message)
        {
            IsConnected();
            await _hubConnection.SendAsync(Messages.UpdateVoting, _username, message);
        }

        public async Task SendStoryAsync(VotingSession message)
        {
            IsConnected();
            await _hubConnection.SendAsync(Messages.UpdateStory, message);
        }

        public async Task StopAsync()
        {
            if (_started)
            {
                await _hubConnection.StopAsync();
                await _hubConnection.DisposeAsync();
                _hubConnection = null;
                _started = false;
            }
        }

        public async ValueTask DisposeAsync()
        {
            MessageReceived = null;

            await StopAsync();
        }

        private void IsConnected()
        {
            if (!_started || _hubConnection.State != HubConnectionState.Connected)
                throw new InvalidOperationException($"Client not started or state is {_hubConnection.State}");
        }
    }
}