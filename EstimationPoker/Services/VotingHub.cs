﻿using System.Threading.Tasks;
using EstimationPoker.Data;
using Microsoft.AspNetCore.SignalR;

namespace EstimationPoker.Services
{
    public class VotingHub : Hub
    {
        // ReSharper disable once UnusedMember.Global
        public async Task UpdateVoting(string user, VotingSession voting)
        {
            await Clients.All.SendAsync(Messages.UpdateVoting, user, voting);
        }

        // ReSharper disable once UnusedMember.Global
        public async Task UpdateStory(VotingSession voting)
        {
            await Clients.All.SendAsync(Messages.UpdateStory, voting);
        }

        // ReSharper disable once RedundantOverriddenMember
        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }
    }
}
