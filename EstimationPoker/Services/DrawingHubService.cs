﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;

namespace EstimationPoker.Services
{
    // https://github.com/conficient/BlazorChatSample/blob/master/BlazorChatSample.Shared/ChatClient.cs
    public class DrawingHubService : IAsyncDisposable
    {
        private readonly string _hubUrl;
        private readonly NavigationManager _navigationManager;
        private readonly string _sessionId;
        private readonly string _username;

        private HubConnection _hubConnection;
        private bool _started;

        public DrawingHubService(string sessionId, string username, NavigationManager navigationManager)
        {
            _sessionId = sessionId;
            _username = username;
            _hubUrl = "/drawinghub";
            _navigationManager = navigationManager;
        }

        public async Task StartAsync()
        {
            if (!_started)
            {
                _hubConnection = new HubConnectionBuilder()
                    .WithUrl(_navigationManager.ToAbsoluteUri(_hubUrl))
                    .Build();

                _hubConnection.On<string, string>(Messages.AddShape,
                    (shape, shapeId) =>
                    {
                            ShapeAdded?.Invoke(this, new SaveShapeEventArgs(shape, shapeId));
                    });

                await _hubConnection.StartAsync();
                _started = true;
            }
        }

        public event SaveShapeEventHandler ShapeAdded;

        public async Task Join(string sessionId)
        {
            IsConnected();
            await _hubConnection.SendAsync(Messages.Join, sessionId);
        }

        public async Task AddShape(string shape, string shapeId)
        {
            IsConnected();
            await _hubConnection.SendAsync(Messages.AddShape, shape, shapeId, _sessionId);
        }

        public async Task StopAsync()
        {
            if (_started)
            {
                await _hubConnection.StopAsync();
                await _hubConnection.DisposeAsync();
                _hubConnection = null;
                _started = false;
            }
        }

        public async ValueTask DisposeAsync()
        {
            ShapeAdded = null;

            await StopAsync();
        }

        private void IsConnected()
        {
            if (!_started || _hubConnection.State != HubConnectionState.Connected)
                throw new InvalidOperationException($"Client not started or state is {_hubConnection.State}");
        }
    }
}