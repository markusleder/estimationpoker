﻿using System;
using EstimationPoker.Data;

namespace EstimationPoker.Services
{
    public delegate void MessageReceivedEventHandler(object sender, MessageReceivedEventArgs e);

    public class MessageReceivedEventArgs : EventArgs
    {
        public MessageReceivedEventArgs(string messageType, string username, VotingSession voting)
        {
            MessageType = messageType;
            Username = username;
            Voting = voting;
        }

        public string MessageType { get; set; }

        public string Username { get; set; }

        public VotingSession Voting { get; set; }
    }

    public delegate void VoteEventHandler(object sender, VoteEventArgs e);

    public class VoteEventArgs : EventArgs
    {
        public VoteEventArgs(int value)
        {
            Value = value;
        }

        public int Value { get; set; }
    }

    public delegate void ModalEventHandler(object sender, VoteEventArgs e);

    public class ModalEventArgs : EventArgs
    {
        public ModalEventArgs(ModalResult value)
        {
            Value = value;
        }

        public ModalResult Value { get; set; }
    }

    public delegate void SaveShapeEventHandler(object sender, SaveShapeEventArgs e);

    public class SaveShapeEventArgs : EventArgs
    {
        public SaveShapeEventArgs(string shape, string shapeId)
        {
            Shape = shape;
            ShapeId = shapeId;
        }

        public string Shape { get; set; }

        public string ShapeId { get; set; }
    }
}

