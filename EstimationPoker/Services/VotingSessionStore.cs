﻿using System.Collections.Generic;
using EstimationPoker.Data;

namespace EstimationPoker.Services
{
    /// <summary>
    /// Simple in-memory voting session store. Does not scale across multiple instances!
    /// </summary>
    public class VotingSessionStore : IVotingSessionStore
    {
        public IDictionary<string, VotingSession> VotingSessions { get; } = new Dictionary<string, VotingSession>();

        public VotingSession CreateNew(string sessionId)
        {
            lock (this)
            {
                if (VotingSessions.TryGetValue(sessionId, out VotingSession votingSession))
                {
                    return votingSession;
                }

                votingSession = new VotingSession {SessionId = sessionId};
                VotingSessions.Add(new KeyValuePair<string, VotingSession>(sessionId, votingSession));
         
                return votingSession;
            }
        }

        public void Remove(string sessionId)
        {
            lock (this)
            {
                if (VotingSessions.ContainsKey(sessionId))
                {
                    VotingSessions.Remove(sessionId);
                }
            }
        }
    }
}
