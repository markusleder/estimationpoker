﻿using EstimationPoker.Data;

namespace EstimationPoker.Services
{
    public interface IVotingSessionStore
    {
        VotingSession CreateNew(string sessionId);

        void Remove(string sessionId);
    }
}