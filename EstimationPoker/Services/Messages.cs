﻿namespace EstimationPoker.Services
{
    public class Messages
    {
        public const string UpdateVoting = "UpdateVoting";
        
        public const string UpdateStory = "UpdateStory";

        public const string AddShape = "AddShape";

        public const string Join = "Join";
    }
}