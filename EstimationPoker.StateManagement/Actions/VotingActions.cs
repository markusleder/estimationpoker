﻿using Redux;

namespace EstimationPoker.StateManagement.Actions
{
    public abstract class VotingAction : IAction
    {
        public string VoterName { get; set; }
    }

    public class JoinVotingSessionAction : VotingAction
    {
    }

    public class ExitVotingSessionAction : VotingAction
    {
    }

    public class ClearVotesAction : IAction
    {
    }

    public class VoteVotingAction : VotingAction
    {
        public int Value { get; set; }
    }

    public class FinalValueAction : VoteVotingAction
    {
    }

    public class UpdateStoryAction : IAction
    {
        public string Story { get; set; }

        public string Description { get; set; }
    }
}
