﻿using System;
using Redux;

namespace EstimationPoker.StateManagement.State
{
    public class VoteState : Immutable<VoteState>
    {
        public string VoterName { get; private set; } = string.Empty;

        public int Value { get; private set; } = None;

        public DateTime Timestamp { get; private set; } = DateTime.MinValue;

        public static int DontKnow => -1;

        public static int None => int.MinValue;

        public VoteState UpdateVoterName(string voterName)
        {
            return Update(s => s.VoterName = voterName);
        }

        public VoteState UpdateValue(int value)
        {
            return Update(s => s.Value = value);
        }

        public VoteState UpdateTimestamp(DateTime timestamp)
        {
            return Update(s => s.Timestamp = timestamp);
        }
    }
}
