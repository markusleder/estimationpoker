﻿using Redux;

namespace EstimationPoker.StateManagement.State
{
    public class ApplicationState : Immutable<ApplicationState>
    {
        public VotingSessionState VotingSession { get; private set; }

        public ApplicationState UpdateVotingSession(VotingSessionState votingSession)
        {
            return Update(s => s.VotingSession = votingSession);
        }
    }
}