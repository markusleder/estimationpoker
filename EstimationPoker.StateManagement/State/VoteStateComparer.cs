﻿using System.Collections.Generic;

namespace EstimationPoker.StateManagement.State
{
    public class VoteStateComparer : IEqualityComparer<VoteState>
    {
        public bool Equals(VoteState x, VoteState y)
        {
            return y != null && (x != null && x.VoterName == y.VoterName);
        }

        public int GetHashCode(VoteState obj)
        {
            return obj.GetHashCode();
        }
    }
}
