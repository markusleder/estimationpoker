﻿using System;
using System.Collections.Immutable;
using System.Linq;
using Redux;

namespace EstimationPoker.StateManagement.State
{
    public class VotingSessionState : Immutable<VotingSessionState>
    {
        public string SessionId { get; private set; } = string.Empty;

        public string Story { get; private set; } = string.Empty;

        public string Description { get; private set; } = string.Empty;

        public ImmutableList<VoteState> Votes { get; private set; } = ImmutableList<VoteState>.Empty;

        public int FinalValue { get; private set; }

        public VotingSessionState UpdateFinalValue(int value)
        {
            return Update(s => s.FinalValue = value);
        }

        public VotingSessionState UpdateSessionId(string sessionId)
        {
            return Update(s => s.SessionId = sessionId);
        }

        public VotingSessionState UpdateStory(string story)
        {
            return Update(s => s.Story = story);
        }

        public VotingSessionState UpdateDescription(string description)
        {
            return Update(s => s.Description = description);
        }

        public VotingSessionState JoinSession(string voterName)
        {
            if (Votes.Any(f => f.VoterName == voterName))
            {
                return this;
            }

            var vote = new VoteState()
                .UpdateVoterName(voterName);
            var newVotes = Votes.Add(vote);
            return Update(s => s.Votes = newVotes);
        }

        public VotingSessionState ExitSession(string voterName)
        {
            if (Votes.All(f => f.VoterName != voterName))
            {
                return this;
            }

            var vote = Votes.Find(v => v.VoterName == voterName);
            var newVotes = Votes.Remove(vote);
            return Update(s => s.Votes = newVotes);
        }

        public VotingSessionState VoteVotingSession(string voterName, int value)
        {
            var vote = Votes.Find(v => v.VoterName == voterName);
            var newVote = vote
                .UpdateValue(value)
                .UpdateTimestamp(DateTime.Now);

            var newVotes = Votes.Replace(vote, newVote);
            return Update(s => s.Votes = newVotes);
        }

        public VotingSessionState ClearVotes()
        {
            var clearedVotes = ImmutableList<VoteState>.Empty;

            foreach (var vote in Votes)
            {
                var newVote = vote
                    .UpdateValue(int.MinValue)
                    .UpdateTimestamp(DateTime.MinValue);
                clearedVotes = clearedVotes.Add(newVote);
            }

            return Update(s => s.Votes = clearedVotes)
                .Update(s => s.FinalValue = int.MinValue);
        }
    }
}