﻿using System;
using System.Collections.Immutable;
using EstimationPoker.StateManagement.Actions;
using EstimationPoker.StateManagement.State;
using Redux;

namespace EstimationPoker.StateManagement.Reducer
{
    public static class Reducers
    {
        public static ApplicationState Reduce(ApplicationState previousState, IAction action)
        {
            return ReduceApplicationState(previousState, action);
        }

        private static ApplicationState ReduceApplicationState(ApplicationState previousState, IAction action)
        {
            var votingSession = ReduceVotingSession(previousState.VotingSession, action);

            return previousState
                .UpdateVotingSession(votingSession);
        }

        private static VotingSessionState ReduceVotingSession(VotingSessionState previousState, IAction action)
        {
            switch (action)
            {
                case FinalValueAction fva:
                    return previousState.UpdateFinalValue(fva.Value);
                case UpdateStoryAction usa:
                    return previousState
                        .UpdateStory(usa.Story)
                        .UpdateDescription(usa.Description);
                case JoinVotingSessionAction jva:
                    return previousState
                        .JoinSession(jva.VoterName);
                case ExitVotingSessionAction eva:
                    return previousState
                        .ExitSession(eva.VoterName);
                case VoteVotingAction vva:
                    return previousState
                        .VoteVotingSession(vva.VoterName, vva.Value);
                case ClearVotesAction cva:
                    return previousState
                        .ClearVotes();
            }

            return previousState;
        }
    }
}
